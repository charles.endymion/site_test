# 1. Retour sur le problème du rendu de monnaie vu en 1ère


## 1.1 Enoncé du problème

On suppose donné un système monétaire. Un commerçant a à sa disposition un nombre illimité 
de pièces de ce système monétaire. Il doit rendre la monnaie à un client en utilisant 
un nombre minimal de pièces. Pour simplifier les choses, on suppose que les valeurs d'un système 
monétaire ainsi que la somme à rendre sont des entiers positifs.

On considèrera trois systèmes monétaires : le système européen, l'ancien système impérial britannique 
et un système imaginaire. On représente chacun d'entre eux par une liste d'entiers, 
chacun de ces entiers correspondant à une valeur du système monétaire : 

 **euros** = [1,2,5,10,20,50] ;   **imperial** =[1,3,6,12,24,30] ;  **imaginaire** = [2,3,9].
 
 

## 1.2 L'algorithme glouton

Pour rappel, l'approche gloutonne une stratégie algorithmique qui consiste à trouver
 une solution à un problème en prenant le meilleur choix à chaque étape 
 et sans revenir sur les choix précédents.

Le principe de l'algorithme du rendu de monnaie glouton est donc le suivant : 

1. on utilise la pièce qui a la plus grande valeur sans dépasser la somme à rendre ; 
2. on actualise la somme à rendre en lui soustrayant cette valeur ;
3. on recommence ainsi jusqu'à ce que la somme à rendre soit égale à $0$.

!!! abstract "Exercice 1.1"  
	Supposons par exemple que la somme à rendre est $49$.

!!! question "1. Quelle est la solution produite par l'algorithme glouton avec le système européen (détailler les étapes). Est-elle optimale ?"
	
??? done "solutions"
	Avec le système européen, l'algorithme glouton nous donne pour solution : $49 = 20 + 20 + 5 + 2 + 2$, soit un nombre de $5$ pièces. 
	On peut démontrer qu'il s'agit bien de la solution optimale.

!!! question "2. Même question dans le cas du système impérial. Est-ce une solution optimale ?"
	
??? done "solutions"
	Avec le système impérial, l'algorithme glouton nous donne pour solution : $49 = 30+12+6+1$, soit un nombre de $4$ pièces. Il ne s'agit ici pas de la solution optimale puisqu'on pouvait utiliser $3$ pièces : $48=24+24+1$.

!!! question "3. Même question dans le cas du système imaginaire. Existe-t-il une solution ?"

??? done "solutions"
	Avec le système imaginaire l'algorithme glouton ne trouve pas de solution : en effet il décompose la somme en $9+9+9+9+9+3 = 48$ 
	et il lui manque donc $1$. Une solution existe pourtant : $9+9+9+9+9+2+2 = 49$

!!! question "4. Que peut-on conclure des résultats précédents ?"

??? done "solutions"
	L'algorithme glouton ne produit pas toujours une solution optimale selon le type de système monétaire.
	Pire encore, il ne trouve même pas de solution dans certains cas alors qu'elle existe.
	
	```python
	def renduGlouton(systeme : list, somme : int) -> int :
    """ Renvoie le nombre minimal de pièces à utiliser pour
        rendre la somme somme avec un système systeme de pièces.
        systeme est une liste d'entiers représentant le système
        (entiers dans l'ordre décroissant).
        Les entiers sont tous positifs."""
    nbpieces = 0
    for piece in systeme :
        while somme >= piece :
            #print(piece)
            somme -= piece
            nbpieces += 1
    if somme > 0 : # pas de solution
        return -1
    return nbpieces
    ```

!!! info "Remarque 1.1" 
	On appelle *système canonique* un système de pièces pour lequel l'algorithme glouton de rendu 
	de monnaie donne une solution optimale quelle que soit la valeur à rendre. 
	Presque tous les systèmes de pièces mondiaux sont canoniques (y compris celui de l'euro). 
	Un contre-exemple historique est le système impérial britannique avant sa réforme en 1971.

## 1.3 Diviser pour régner : un algoritme récursif naïf

Pour obtenir la solution optimale, on peut utiliser un algorithme récursif qui explore toutes les sommes inférieures ou égales au rendu réalisables avec un ensemble de pièces donné 
et renvoie le nombre minimal de pièces (renvoie $+\infty$ quand il n’y a pas de solution) :

![](img/prog_1.PNG)

!!! abstract "Exercice 1.2"

!!! question "Décrire en quoi cette approche est une application de la méthode Diviser pour régner."

??? done "solutions" 
	On divise le problème en $n$ sous-problèmes où $n$ désigne le nombre de pièce de type différent du système monétaire choisi. 
	Par exemple, dans le cas du système imaginaire pour la somme $49$, on découpe la recherche en trois sous-problème 
	avec les sommes $49-9 = 40$, $49-3=46$ et $49-2 = 47$.  Le fait de prendre le minimum entre le nombre de pièces déjà calculé et le nombre de pièces obtenus 
	par le sous-problème permet de combiner de régner sur les sous-problèmes en combinant les solutions. 
	Le raisonnement est le suivant : si *v* est une valeur du système monétaire, alors pour rendre la somme *somme* de façon optimale en utilisant au moins une fois la pièce *v* , il suffit de rendre *v* et de rendre la somme *somme-v* 
	de façon optimale. On s'est ainsi ramené au même problème, mais avec une somme à rendre plus petite. En appliquant ce raisonnement pour toutes les pièces possibles et en choisissant la solution minimale parmi ces solutions, 
	on obtient la solution optimale du problème de départ.
	
Cette méthode fonctionne en effectuant toutes les combinaisons possibles de pièces pour une certaine somme et à choisir celle qui comporte le moins de pièces. Examinons plus en détails les appels récursifs réalisés.

!!! abstract "Exercice 1.3"
	On considère le système de pièces imaginaire $[9,3,2]$ et on fait fonctionner l'algorithme récursif naïf sur la somme $10$.

!!! question "1. Construire l'arbre des appels récursifs (n'écrire que les sommes sur lesquelles la fonction `#!py3 renduRec` est appelée)"
	
??? done "solutions"
	Pour des questions de mise en page, 1 a été placé entre 8 et 7, il devrait être placé avant 7.
	![](img/prog_2.PNG)

!!! question "2. Que peut-on constater en examinant cet arbre d'appel ?"

??? done "solutions"
	Il y a beaucoup de répétitions (mises en couleur) : il y a deux fois le même sous-arbre de sommet 4 et de sommet 5, 3 fois celui de sommet 3, 4 fois celui de sommet 2.
	```python
	def renduNaifRecursif(systeme : list, somme : int) -> int :
    """ Renvoie le nombre minimal de pièces à utiliser pour
        rendre la somme somme avec un système systeme de pièces donné.
        systeme est une liste d'entiers représentant le système
        (entiers dans l'ordre décroissant).
        Les entiers sont tous positifs.
        L'algorithme renvoie un nombre > 10000 si pas de solution."""
    if somme == 0 : # cas de base
        return 0
    nbpieces = 10000 # infini...
    for piece in systeme :
        if piece <= somme :
            #print(piece)
            nbpieces = min(nbpieces, 1+renduNaifRecursif(systeme, somme-piece))
    return nbpieces
	```
## 1.4 Programmation dynamique du rendu de monnaie

Comme on a pu le voir précédemment, la programmation récursive de la résolution de ce problème permet d'obtenir la solution optimale mais elle nécessite de nombreux appels identiques. L'une des clés de la programmation dynamique est d'éviter ces calculs redondants. 

Il y a plusieurs techniques pour cela.

Une idée pour éviter de recalculer plusieurs fois le même résultat est de mémoriser les résultats calculés dans un tableau dédié. On choisit de calculer tous les résultats des sous-problèmes, en commençant par les plus simples et en finissant par les plus compliqués, ce qui permet de supprimer la récursivité.

Dans notre exemple du rendu de monnaie pour la somme de $49$ euros, il s'agit de calculer dans cet ordre toutes les solutions optimales pour les sommes allant de $0$ à $49$ euros inclus.

On donne l'algorithme suivant :

![](img/prog_3.PNG)

!!! abstract "**Exercice 1.4**"

!!! question "1. Exécuter à la main l'appel `#!py3 renduDyna([2,1],5)`."

??? done "solutions"
    ![](img/prog_4.PNG)

!!! question "2.Implémenter cette fonction en *Python* et la tester avec la somme 49 pour les trois systèmes monétaires proposés."

??? done "solutions"
    ```python
    def renduDyna(systeme : list, somme : int) -> int :
    nb = [k for k in range(0,somme+1)]
    for s in range(1,somme+1) :
        for p in systeme :
            if p<= s :
                nb[s] = min(nb[s],1+nb[s-p])
                #print("s =",s, "\tp =", p, "\t nb = ",nb[s])
    return nb[somme]
    ```
	On obtient les résultats suivants :
	
	Rendu dynamique pour le système  [50, 20, 10, 5, 2, 1]  et la somme  49  :  5
	
	Rendu dynamique pour le système  [30, 24, 12, 6, 3, 1]  et la somme  49  :  3
	
	Rendu dynamique pour le système  [9, 3, 2]  et la somme  49  :  7

!!! question "3.Que renvoie l'instruction `#!py3 renduDyna([9,3,2],10)` ? La réponse est-elle correcte ? Pourquoi cela se produit-il ? Comment pourrait-on y remédier ?"

??? done "solutions"
    L'algorithme renvoie un nombre de pièces dans tous les cas même s'il n y a pas de solution car on a initialisé le tableau avec les valeurs de $0$ à `#!py3 somme` comme si la pièce de valeur $1$ était forcément présente dans notre système. Pour la somme 10 avec ce système imaginaire la solution est 4 (deux pieces de 2 et deux pièces de 3). L'algorithme renvoie cependant $2$. Pour corriger ce défaut il faudrait une seconde liste mémorisant le total atteint avec la combinaison obtenue et verifier que le total est bien égal à la somme demandée
	(on pourra alors aussi renvoyer -1 pour signifier que le rendu est impossible par exemple).
	
!!! info "Remarque 1.2 "
	Dans cette fonction, on a deux boucles emboitées qui contiennent une opération en temps constant (calcul d'un minimum). Le temps d'exécution est alors proportionnel au produit du nombre de pièces du système par la somme.

	C'est plus long que pour l'algorithme glouton mais on obtient une solution optimale avec tous les systèmes de pièces qui contiennent la pièce de valeur $1$ (n'importe quel rendu d'une somme entière est alors possible). Par contre, cette amélioration nécessite un plus grand espace mémoire avec le tableau.

	Dans le code précédent, on calcule le nombre de pièces correspondant à la solution optimale, mais on ne précise pas comment cette solution a été obtenue.

On propose ci-dessous une fonction en *Python* permettant non seulement de calculer le nombre de pièces utiles mais aussi la combinaison des pièces à rendre et on gère également le cas où le rendu est impossible.

```python	
def renduDynaCombi(systeme : list, somme : int) -> list :
    """ Renvoie une liste minimale de pièces constituant la combinaison
        des pièces à rendre pour le rendu de la somme donnée avec le
        système de pièces donné.
        La fonction renvoie [-1] quand le rendu est impossible."""
    combi = [[0 for k in range(s)] for s in range(0,somme+1)]
    for s in range(1,somme+1) :
        for piece in systeme :
            if piece <= s :
                if len(combi[s]) > 1+len(combi[s-piece]) or 0 in combi[s] :
                    combi[s] = combi[s-piece] + [piece]
        print((s,piece), "\tcombi =",combi[s])
    if 0 in combi[somme] : # somme impossible à réaliser
        return [-1]
    return combi[somme]
```

!!! abstract "**Exercice 1.5**"

!!! question "1.Expliquer la ligne 6."

??? done "solutions"
	Le tableau `#!py3 combi` est initialisé avec des listes composées d'autant de 0 que la valeur de `#!py3 s` 
    (variant de 0 à `#!py3 somme` inclus).

!!! question "2.Expliquer le test de la ligne 10."

??? done "solutions"
	Quand la valeur de la pièce est inférieure ou égale à `#!py3 s`  on teste si le nombre de
	pièces de la combinaison de `#!py3 s`  est supérieure au nombre des pièces de la combinaison de la somme `#!py3 s-piece`
	augmenté de 1 mais aussi si 0 est est encore présent dans la combinaison. 
	Cela permet de tester si on a trouvé une meilleure combinaison mais aussi si une réelle combinaison a été trouvée.

!!! question "3. Que renvoie la fonction quand on l'exécute avec le système imaginaire et la somme 10 ? Expliquer."

??? done "solutions"
	On construit la combinaison pour la somme 10. Au départ elle est composée de 10 fois le nombre 0. 
	Quand on atteint la valeur 10 dans la boucle on constate que la valeur de la pièce 9 est inférieure 
	ou égale à 10 et comme `#!py3 len(combi[10])`  vaut 10 et que `#!py3 combi[1]=[0]` (pas de solution pour 1) on change la valeur de `#!py3 combi[10]` en `#!py3 [0]+[9]`.
	Comme 0 est encore présent dans la combinaison trouvée, on traite la pièce 3 :
	la combinaison est mise à jour avec `#!py3 combi[8]+[3]`  où `#!py3 combi[8]=[2,2,3]`. 
	Pas de changement pour la pièce de valeur 2. Le résultat renvoyé est bien la solution optimale `#!py3 [2,2,3,3]`