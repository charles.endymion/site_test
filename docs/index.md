# Welcome to MkDocs yo55

For full documentation visit yo [mkdocs.org](https://www.mkdocs.org).

![Bernie](img/Bernie2.PNG)

!!! note "Une question pour vous "
	Quel est la réponse à la question universelle ?

??? done "solution"
	Bravo tu as trouvé !
	
=== "Tab 1"
    Markdown **content**.

    Multiple paragraphs.

=== "Tab 2"
    More Markdown **content**.

    - list item a
    - list item b
	
:smile: :heart: :thumbsup:

And here is a comment on {==some
 text==}{>>This works quite well. I just wanted to comment on it.<<}. Substitutions {~~is~>are~~} great!

General block handling.

???+ note "Open styled details"

    ??? danger "Nested details!"
        And more content again.
		
??? success
    Content.

??? warning classes
    Content.

++ctrl+alt+delete++

Task List

- [X] item 1
    * [X] item A
    * [ ] item B
        more text
        + [x] item a
        + [ ] item b
        + [x] item c
    * [X] item C
- [ ] item 2
- [ ] item 3
	
My favourite web framework is *[Django][1]*.

The **[Django][1]** framework provide a [`template language`][2].

[1]: https://www.djangoproject.com/
[2]: https://docs.djangoproject.com/en/3.1/ref/templates/language/

![Linux Penguin][3]

[3]: http://pageperso.lif.univ-mrs.fr/~edouard.thiel/images/penguineyes.gif "Dancing penguin"


Here is a simple footnote,[^1] and here is a longer one.[^bignote]

[^1]: This is the first footnote.

[^bignote]: Here is one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.	
	
[Une console Basthon](https://console.basthon.fr/){ .md-button }

[Une console Basthon](https://console.basthon.fr/){ .md-button .md-button--primary }

--8<-- "README.md"


```python linenums="1"
--8<--- "docs/scripts/hello.py"
```

=== "Numérotation classique"
    !!! note "Entrée"
        ````markdown
        ```python linenums="1"
        --8<--- "docs/scripts/hello.py"
        ```
        ````
    
    !!! done "Rendu"
        ```python linenums="1"
        --8<--- "docs/scripts/hello.py"
        ```
=== "Marquage lignes et tranches"
    !!! note "Entrée"
        ````markdown
        ```python linenums="1" hl_lines="1 2 3-3 5-5"
        --8<--- "docs/scripts/hello.py"
        ```
        ````
    
    !!! done "Rendu"
        ```python linenums="1" hl_lines="1 2 3-3 5-5"
        --8<--- "docs/scripts/hello.py"
        ```

❤️
😘😘
Je pars :surfer:
